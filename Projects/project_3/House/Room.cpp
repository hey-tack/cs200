#include "Room.hpp"

#include <iostream>
using namespace std;

void Room::Setup( string name, float width, float length, float height )
{
    m_name = name;
    m_width = width;
    m_length = length;
    m_height = height;
}

void Room::UserSetup()
{
    cout << "Please enter values for the rooms name, width, length, and height" << endl;
    cout << "Name: ";
    cin >> m_name;
    cout << endl;
    cout << "Width: ";
    cin >> m_width;
    cout << endl;
    cout << "Length: ";
    cin >> m_length;
    cout << endl;
    cout << "Height: ";
    cin >> m_height;
    cout << endl;
}

string Room::GetName()
{
    return m_name;
}

float Room::GetWidth()
{
    return m_width;
}

float Room::GetLength()
{
    return m_length;
}

float Room::GetHeight()
{
    return m_height;
}

float Room::GetSquareFootage()
{
    return m_length * m_width;
}

void Room::Display()
{
    cout << " Name: " << m_name << " Square Footage: " << GetSquareFootage() << endl;
}

