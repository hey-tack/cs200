#include "House.hpp"

#include <iostream>
#include <fstream>
using namespace std;

House::House()
    : MAX_ROOMS( 10 )
{
    m_totalRooms = 0;
    LoadHouse();
}

House::~House()
{
    SaveHouse();
}

bool House::IsFull()
{
    if (m_totalRooms == MAX_ROOMS) {
        return true;
    } else {
        return false;
    }
}

bool House::IsValidIndex( int index )
{
    if (index < m_totalRooms && index >= 0) {
        return true;
    } else {
        return false;
    }
}

int House::GetRoomCount()
{
    return m_totalRooms;
}

void House::AddRoom()
{
    if (!IsFull()) {
        m_roomArray[m_totalRooms].UserSetup(); // Ask the user for information about the room
        m_totalRooms++;
    }
}

float House::GetTotalSquareFootage()
{
    float totalSquareFootage = 0;
    for (int i = 0; i < m_totalRooms; i++) {
        totalSquareFootage += m_roomArray[i].GetSquareFootage();
    }
    return totalSquareFootage;
}

void House::Display()
{
    for (int i = 0; i < m_totalRooms; i++) {
        cout << "Index: " << i;
        m_roomArray[i].Display();
    }
    cout << "Total Square Footage: " << GetTotalSquareFootage() << endl;
}

void House::UpdateRoom()
{
    int roomToUpdate = -1;
    Display();
    cout << "What room would you like to update?" << endl;
    cout << "Room Number: ";
    cin >> roomToUpdate;

    if (IsValidIndex(roomToUpdate)) {
        m_roomArray[roomToUpdate].UserSetup(); // Ask the user for details about the room;
    } else {
        cout << "Invalid index. Please select a valid room number next time." << endl;
    }
}

void House::SaveHouse()
{
    // This is already implemented
    ofstream output( "house.txt" );
    for ( int i = 0; i < m_totalRooms; i++ )
    {
        output << "ROOM_BEGIN" << endl;
        output  << "NAME" << endl
                << m_roomArray[i].GetName() << endl;
        output  << "WIDTH" << endl
                << m_roomArray[i].GetWidth() << endl;
        output  << "LENGTH" << endl
                << m_roomArray[i].GetLength() << endl;
        output  << "HEIGHT" << endl
                << m_roomArray[i].GetHeight() << endl;
        output  << "ROOM_END" << endl << endl;
    }
}

void House::LoadHouse()
{
    // This is already implemented
    ifstream input( "house.txt" );

    string buffer;
    string name;
    int width, length, height;

    while ( getline( input, buffer ) )
    {
        if ( buffer == "NAME" )
        {
            getline( input, name );
        }
        else if ( buffer == "WIDTH" )
        {
            input >> width;
        }
        else if ( buffer == "LENGTH" )
        {
            input >> length;
        }
        else if ( buffer == "HEIGHT" )
        {
            input >> height;
        }
        else if ( buffer == "ROOM_END" )
        {
            m_roomArray[ m_totalRooms ].Setup( name, width, length, height );
            m_totalRooms++;
        }
    }
}
