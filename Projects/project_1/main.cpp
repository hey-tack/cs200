#include <iostream>     // Used for cin and cout
#include <iomanip>      // Used for formatting cout statements
#include <string>       // Used for string data types
#include <cstdlib>      // Used for rand()
#include <ctime>        // time used to seed random number generator
using namespace std;

void DisplayMenu( int day, int food, int health, int maxHealth, string name, string location );

int main()
{
    /******************************************************************* INITIALIZE VARIABLES */
    bool done = false;                      // Flag for if the game is over
    bool successfulAction = false;          // Flag for if the user entered valid input
    int food = 5;                           // Player stat - how much food they have
    int maxHealth = 20;                     // Player stat - their maximum amount of health
    int health = maxHealth;                 // Player stat - their current health amount
    string name = "ME";                     // Player stat - what their name is
    string location = "Overland Park";      // Player stat - what location they're in
    int day = 1;                            // Player stat - how many days it has been

    string dump;                            // A variable to dump input into
    int choice;                             // A variable to dump input into
    int mod;                                // A variable used for calculations later

    srand( time( NULL ) );                  // Seeding the random number generator

    /******************************************************************* GAME START */
    cout << "Z O M B I E  -  A P O C A L Y P S E" << endl;
    cout << "Enter your name: ";
    getline( cin, name );

    cout << endl;

    /******************************************************************* GAME LOOP */
    while ( !done )
    {
        /** Start of the round **/
        successfulAction = false;
        DisplayMenu( day, food, health, maxHealth, name, location );
        cout << "CHOICE: ";
        cin >> choice;

        /** Student implements features here **/
        // Player Action Stuff
        switch (choice) {
            case 1: // Scavenge
                {
                int randomChance = rand() % 5; // 0-4
                int mod;
                successfulAction = true; // Will be used next iteration?
                cout << "You attempt to scavenge here." << endl;
                switch (randomChance) {
                    case 0: // Found food stash
                        mod = rand() % 3 + 2; // 2-5
                        cout << "You find a stash of food (+" << mod << " food)" << endl;
                        food += mod;
                        break;
                    case 1: // Zombie surprise
                        cout << "A zombie surprises you!" << endl;
                        mod = rand() % 6 + 2; // 2-8
                        cout << "You get hurt in the encounter. (-" << mod << " health)" << endl;
                        health -= mod;
                        break;
                    case 2: // Find medical supplies
                        mod = rand() % 3 + 2; // 2-5
                        cout << "You have found some medical supplies." << endl;
                        if (health == maxHealth) { // decided to add a little extra logic for max health.
                            cout << "However, you're in perfect health, and you have no pockets." << endl;
                            cout << "You reluctantly leave them behind." << endl;
                        } else {
                            cout << "Thank goodness. You tend to your wounds. (+" << mod << " health)" << endl;
                        }
                        health += mod;
                        break;
                    case 3: // Ambushed
                        mod = rand() % 4 + 2; // 2-6
                        cout << "Another scavenger ambushes you! taking some of your supplies. (-" << mod << " food)" << endl;
                        food -= mod;
                        break;
                    case 4: // Nothing
                        cout << "You don't find anything." << endl;
                        break;
                }
                break;
            }
            case 2: // Moving
                cout << "Walk to where?" << endl;
                cout << "1. Overland Park" << endl;
                cout << "2. Raytown" << endl;
                cout << "3. Kansas City" << endl;
                cout << "4. Gladstone" << endl;
                cin >> choice;

                switch(choice) {
                    case 1:
                        if (location == "Overland Park") {
                            cout << "You're already here!" << endl;
                        } else {
                            location = "Overland Park";
                            successfulAction = true;
                        }
                    break;
                    case 2:
                        if (location == "Raytown") {
                            cout << "You're already here!" << endl;
                        } else {
                            location = "Raytown";
                            successfulAction = true;
                        }
                    break;
                    case 3:
                        if (location == "Kansas City") {
                            cout << "You're already there!" << endl;
                        } else {
                            location = "Kansas City";
                            successfulAction = true;
                        }
                    break;
                    case 4:
                        if (location == "Gladstone") {
                            cout << "You're already there!" << endl;
                        } else {
                            location = "Gladstone";
                            successfulAction = true;
                        }
                    break;
                    default:
                        cout << "Invalid selection!" << endl;
                    break;
                }

                if (successfulAction) {
                    cout << "You travel to " << location << "." << endl;
                    int randomChance = rand() % 5; // 0-4
                    switch(randomChance) {
                        case 0: // Zombie fight 1
                            cout << "A zombie attacks!" << endl;
                            cout << "You fight it off." << endl;
                        break;
                        case 1: // Zombie fight 2
                            mod = rand() % 4 + 2; // 2-6
                            cout << "A zombie attacks!" << endl;
                            cout << "It bites you as you fight it! (-" << mod << " health)" << endl;
                            health -= mod;
                        break;
                        case 2: // Trade
                            mod = rand() % 2 + 2; // 2-4
                            cout << "You find another scavenger, and trade goods. (+" << mod << " food)" << endl;
                            food += mod;
                        break;
                        case 3:
                            mod = rand() % 3 + 2; // 2-5
                            cout << "You find a safe building to rest in temporarily. (+" << mod << " health)" << endl;
                            health += mod;
                        break;
                        case 4: // Uneventful
                            cout << "The journey is uneventful." << endl;
                        break;
                    }
                } else {
                    continue;
                }
            break;
            default:
                cout << "Invalid selection!" << endl;
                continue; // Skip back to input
            break;

        }

        // Player State Stuff
        cout << "The day passes (+1 day)." << endl;
        day++;

        if (food > 0) {
            cout << "You eat a meal (-1 food)." << endl;
            food--;
            health++;
        } else {
            mod = rand() % 4 + 1;
            cout << "You are starving! (-" << mod << " health)" << endl;
            health -= mod;
        }

        if (health > maxHealth) {
            health = maxHealth;
        }

        if (food <= 0) {
            food = 0;
        }

        if (health < 1) {
            cout << "You have died.";
            done = true;
        } else if (day > 19) {
            cout << "In the morning, a group of scavengers find you. " << endl;
            cout << "They have a fortification nearby and are rebuilding a society." << endl;
            cout << "You agree to live in their town." << endl;
            done = true;
        }

        /** End of the round - nothing to update **/
        cout << endl << "Press ENTER to continue...";
        cin.ignore();
        getline( cin, dump );

        cout << "----------------------------------------" << endl;
        cout << "----------------------------------------" << endl;
    }

    /******************************************************************* GAME OVER */
    cout << endl << endl << "You survived the apocalpyse on your own for " << day << " days." << endl;

    return 0;
}

/* You don't need to modify this, it just displays the user's information at the start of each round. */
void DisplayMenu( int day, int food, int health, int maxHealth, string name, string location )
{
    cout << "----------------------------------------" << endl;
    cout << left << setw( 3 ) << "-- " << setw( 35 ) << name << "--" << endl;
    cout << left
        << setw( 6 ) << "-- Day "   << setw( 4 ) << day
        << setw( 6 ) << "Food: "    << setw( 4 ) << food
        << setw( 8 ) << "Health: "  << setw( 2 ) << health << setw( 1 ) << "/" << setw( 2 ) << maxHealth
        << right << setw( 6 ) << "--" << endl;
    cout << left
        << setw( 10 ) << "-- Location: " << setw( 20 ) << location
        << right << setw( 7 ) << "--" << endl;
    cout << "----------------------------------------" << endl;
    cout << "-- 1. Scavenge here                   --" << endl;
    cout << "-- 2. Travel elseware                 --" << endl;
    cout << "----------------------------------------" << endl;
    for ( int i = 0; i < 15; i++ )
    {
        cout << endl;
    }
}
