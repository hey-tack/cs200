
#include <iostream>
#include <string>
#include <windows.h> // For color stuff
using namespace std;

// For loops lab 1: Count up
void Program1()
{
    for (int i = 0; i <= 20; i++) {
        cout << i << " ";
    }
}

// For loops lab 2: Multiply up
void Program2()
{
    for (int i = 1; i <= 128; i *= 2) {
        cout << i << " ";
    }
}

// For loops lab 2: Summation
void Program3()
{
    int sum;
    int n;

    cout << "Please enter a value for n: ";
    cin >> n;

    for (int i = 0; i <= n; i++) {
        sum += i;
        cout << "Sum: " << sum << endl;
    }
}

// For loops lab 4: Strings and characters
void Program4()
{
    // For console color stuff, just for fun
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    int COLOR_GREEN = 10;
    int COLOR_DEFAULT = 7;

    string text;
    char letterToCount;
    int letterCount;

    cout << "Enter a string: ";
    cin.ignore();
    getline(cin, text);

    cout << "Enter a letter to count: ";
    cin >> letterToCount;

    for (int i = 0; i < text.size(); i++) {
        if (text[i] == letterToCount) {
            letterCount++;
            SetConsoleTextAttribute(hConsole, COLOR_GREEN);
        }
        cout << "Letter " << i << ": " << text[i] << endl;
        SetConsoleTextAttribute(hConsole, COLOR_DEFAULT);
    }

    cout << endl;
    cout << "There are " << letterCount << " " << letterToCount << "(s) in \"" << text << "\"";
}

int main()
{
    // Don't modify main
    while ( true )
    {
        cout << "Run which program? (1-4): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
