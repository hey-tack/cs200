#ifndef _SHAPES_PROGRAM_HPP
#define _SHAPES_PROGRAM_HPP

#include "Shapes.hpp"

class ShapesProgram
{
    public:
    void Run()
    {
        cout << endl << endl << "----------- SHAPES -----------" << endl;

        cout << endl << "UNITS" << endl;
        cout << "Enter unit of measurement: ";
        string unit;
        cin >> unit;

        cout << endl << "CIRCLE" << endl;
        cout << "Enter radius: ";
        float radius;
        cin >> radius;

        Circle circle;
        circle.Setup( radius, unit );
        cout << "Area is: ";
        circle.DisplayArea();

        cout << endl << "RECTANGLE" << endl;
        cout << "Enter width and height: ";
        float width, height;
        cin >> width >> height;

        Rectangle rectangle;
        rectangle.Setup( width, height, unit );
        cout << "Area is: ";
        rectangle.DisplayArea();
    }
};

#endif
