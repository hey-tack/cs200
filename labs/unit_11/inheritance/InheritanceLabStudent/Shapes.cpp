#include "Shapes.hpp"
#include <math.h> // for pow


/* RECTANGLE ***********************************************************/
/***********************************************************************/

void Rectangle::Setup( float width, float height, string unit )
{
    Shape::Setup(unit);
    m_width = width;
    m_height = height;
}

void Rectangle::DisplayArea()
{
    float area = CalculateArea();
    cout << "The area of the rectangle is " << area << " " << m_unit;
}

float Rectangle::CalculateArea()
{
    return m_width * m_height;
}

/* CIRCLE **************************************************************/
/***********************************************************************/

void Circle::Setup( float radius, string unit )
{
    Shape::Setup(unit);
    m_radius = radius;
}

void Circle::DisplayArea()
{
    float area = CalculateArea();
    cout << "The are of the circle is " << area << " " << m_unit;
}

float Circle::CalculateArea()
{
    return 3.14 * pow(m_radius,2);
}

/* SHAPE (parent class) FUNCTIONS **************************************/
/***********************************************************************/

void Shape::Setup( string unit )
{
    m_unit = unit;
}

void Shape::DisplayArea()
{
    cout << " " << m_unit << "^2";
}

float Shape::CalculateArea()
{
    return 0;
}
