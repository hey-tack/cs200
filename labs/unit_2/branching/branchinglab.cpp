#include <iostream> // Use cin and cout
#include <string>   // Use strings
using namespace std;

// Branching lab 1: Hometown
void Program1() {
    string name;
    string hometown;

    cout << "Enter your hometown: ";
    cin >> hometown;
    cout << endl;

    if (hometown.size() > 6) {
        cout << "That's a long name!" << endl;
    }

    cout << "Enter your name: ";
    cin >> name;
    cout << endl;

    cout << "Hello " << name << " from " << hometown << "!" << endl;
}

// Branching lab 2: Pass/fail
void Program2() {
    float userPoints;
    float totalPoints;
    float grade;

    cout << "How many points was your assignment eligible for?: ";
    cin >> totalPoints;
    cout << endl;

    cout << "How many points did you score?: ";
    cin >> userPoints;
    cout << endl;

    // calculate grade
    grade = (userPoints / totalPoints) * 100;

    cout << "Your grade is: " << grade << endl;

    if (grade >= 60) {
        cout << "You passed!";
    } else if ( grade < 60) {
        cout << "You failed!";
    }
}

// Branching lab 3: Battery charge
void Program3() {
    int charge;

    cout << "Please enter your phone's charge percentage (1-100): ";
    cin >> charge;
    cout << endl;

    if (charge >= 75) {
        cout << "[****]";
    }
    else if (charge >= 50) {
        cout << "[***_]";
    }
    else if (charge >= 25) {
        cout << "[**__]";
    }
    else if (charge >= 5) {
        cout << "[*___]";
    }
    else {
        cout << "[____]";
    }
}

// Branching lab 4: Input validation
void Program4() {
    int userChoice;

    cout << "What is your favorite pasta?" << endl;
    cout << "1. Rigatoni" << endl;
    cout << "2. Linguine" << endl;
    cout << "3. Spaghetti" << endl;
    cout << "4. Fettuccine" << endl;

    cin >> userChoice;

    if (userChoice > 0 && userChoice <= 4) {
        cout << "Good choice!";
    } else {
        cout << "Invalid choice!";
    }
}

// Branching lab 5: Calculator
void Program5() {
    float num1;
    float num2;
    float result;
    char mathOperator;

    cout << "Enter your first number: ";
    cin >> num1;
    cout << endl;

    cout << "Enter your second number: ";
    cin >> num2;
    cout << endl;

    cout << "What type of operation? (+ - * /): " << endl;
    cout << "+: add" << endl;
    cout << "-: subtract" << endl;
    cout << "*: multiply" << endl;
    cout << "/: divide" << endl;
    cout << "Choice: ";

    cin >> mathOperator;
    cout << endl;

    switch (mathOperator) {
        case '+':
            result = num1 + num2;
            cout << "Result: " << result;
            break;
        case '-':
            result = num1 - num2;
            cout << "Result: " << result;
            break;
        case '*':
            result = num1 * num2;
            cout << "Result: " << result;
            break;
        case '/':
            if (num2 == 0) {
                cout << "Invalid operation!" << endl;
            } else {
                result = num1 / num2;
                cout << "Result: " << result;
            }
            break;
        default:
            cout << "Invalid operation!" << endl;
    }
}

// Don't modify main
int main() {
    while (true) {
        cout << "Run which program? (1-5)";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1) { Program1(); }
        else if ( choice == 2) { Program2(); }
        else if ( choice == 3) { Program3(); }
        else if ( choice == 4) { Program4(); }
        else if ( choice == 5) { Program5(); }

        cout << endl << "-------------------" << endl;
    }

    return 0;
}
