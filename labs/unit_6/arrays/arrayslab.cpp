#include <iostream>
#include <fstream>
#include <string>
using namespace std;

void Program1() // Course Names
{
    const int MAX_COURSES = 6;
    int courseCount = 0;
    string myCourses[MAX_COURSES];

    cout << "CLASS LIST" << endl << endl;

    // Build list of courses
    for(int i = 0; i < MAX_COURSES; i++) {
        string userInput;
        cout << "Enter name for class " << i << " or type STOP: ";
        cin >> userInput;

        if (userInput == "STOP") {
            break;
        } else {
            myCourses[i] = userInput;
            courseCount++;
        }
    }

    cout << endl;

    // Display build course list
    cout << "Your classes: " << endl << endl;

    for (int i = 0; i < MAX_COURSES; i++) {
        cout << myCourses[i] << "\t";
    }

    cout << endl << endl;
}

void Program2() // Two Arrays
{
    const int MAX_SIZE = 5;
    string names[MAX_SIZE];
    string prices[MAX_SIZE];

    cout << "STOREFRONT" << endl << endl;

    for (int i = 0; i < MAX_SIZE; i++) {
        cout << "ITEM " << (i+1) << endl;
        cout << "Enter item's name: ";
        cin >> names[i];
        cout << endl;
        cout << "Enter item's price: ";
        cin >> prices[i];
        cout << endl;
    }

    ofstream output;
    output.open("items.txt");

    output << "ITEMS FOR SALE" << endl << endl;

    for (int i = 0; i < MAX_SIZE; i++) {
        output << (i+1) << " " << names[i] << " $" << prices[i] << endl;
    }

    output.close();
}

void Program3() // Using 2D array
{
    // This is to convert numbers to day names easier
    const string daysOfWeek[7] {
        "Sunday", "Monday", "Tuesday", "Wednesday",
        "Thursday", "Friday", "Saturday"
    };

    const int DAYS_IN_WEEK = 7;
    const int HOURS_IN_DAY = 24;
    // This is our to do list array
    string todo[DAYS_IN_WEEK][HOURS_IN_DAY];

    int toDoItemCount = 0;


    bool done = false;

    while(!done) {
        int userInput = 0;
        cout << "TO DO LIST" << endl << endl;

        cout << "1. Add new to-do item" << endl;
        cout << "2. Save list" << endl;
        cout << "3. Quit" << endl;
        cin >> userInput;

        switch(userInput) {
            case 1: {
                int dayOfWeek = 0;
                int timeOfDay = 0;
                cout << endl;
                cout << "NEW ITEM" << endl << endl;
                cout << "0. Sunday \t 1. Monday" << endl;
                cout << "2. Tuesday \t 3. Wednesday" << endl;
                cout << "4. Thursday \t 5. Friday" << endl;
                cout << "6. Saturday" << endl << endl;

                cout << "Which day of the week? (0 to 6): ";
                cin >> dayOfWeek;
                cout << endl;

                cout << "Which time of day? (0 to 23): ";
                cin >> timeOfDay;
                cout << endl;

                cout << "What is the to do item?: ";
                cin.ignore();
                getline(cin, todo[dayOfWeek][timeOfDay]);

                cout << endl << endl;

                cout << "ADDED" << endl << endl;
                break;
            }
            case 2: {
                string fileName;
                ofstream output;

                cout << endl;
                cout << "SAVE LIST" << endl << endl;
                cout << "What name would you like you file to have?: ";
                cin >> fileName;
                cout << endl;

                output.open(fileName + ".txt");

                // Loop through all todos and output to file;
                for (int day = 0; day < DAYS_IN_WEEK; day++) {
                    output << daysOfWeek[day] << endl;
                    for (int hour = 0; hour < HOURS_IN_DAY; hour++) {
                        if (todo[day][hour] != "") {
                            output << "\t" << hour << ":00 \t" << todo[day][hour] << endl;
                        }
                    }
                    output << endl;
                }

                break;
            }
            case 3: {
                cout << "Bye bye!" << endl << endl;
                done = true;
                break;
            }
        }

    }
}

int main()
{
    bool done = false;

    while ( !done )
    {
        int choice;
        cout << "0. QUIT" << endl;
        cout << "1. Program 1" << endl;
        cout << "2. Program 2" << endl;
        cout << "3. Program 3" << endl;
        cout << endl << ">> ";
        cin >> choice;

        switch( choice )
        {
            case 0: done = true; break;
            case 1: Program1(); break;
            case 2: Program2(); break;
            case 3: Program3(); break;
        }
    }

    return 0;
}

