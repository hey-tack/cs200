#include <iostream>
using namespace std;

// Functions lab 1, Percent to decimal.

// Input: A percent value
//        (0% - 100%, no percent sign though)
// Output: The decimal equivalent (0.0 - 1.0)
float PercentToDecimal(float percent) {
    float decimal = percent / 100;
    return decimal;
}

int main () {
    float userPercent;
    float decimalVersion;
    cout << "PERCENT TO DECIMAL" << endl << endl;
    cout << "Enter a percent value, without the %: ";
    cin >> userPercent;
    cout << endl;
    decimalVersion = PercentToDecimal(userPercent);
    cout << "Decimal value: " << decimalVersion << endl;
    return 0;
}
