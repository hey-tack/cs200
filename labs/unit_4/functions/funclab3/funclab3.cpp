#include <iostream>
#include <string>
using namespace std;

// Function lab 3: Count change

float CountChange(
  int quarterCount,
  int dimeCount,
  int nickelCount,
  int pennyCount) {
    return
    (quarterCount * 0.25)
    + (dimeCount * 0.10)
    + (nickelCount * 0.05)
    + (pennyCount * 0.01);
}

int main() {
    cout << "COUNT CHANGE" << endl << endl;

    while(true) {
        int quarters, dimes, nickels, pennies;

        // 1. Get user input for the amount of each type of change
        cout << "How many quarters? ";
        cin >> quarters;
        cout << endl;

        cout << "How many dimes? ";
        cin >> dimes;
        cout << endl;

        cout << "How many nickels? ";
        cin >> nickels;
        cout << endl;

        cout << "How many pennies? ";
        cin >> pennies;
        cout << endl;

        // 2. Call the function and store the result
        float countedAmount = CountChange(quarters, dimes, nickels, pennies);

        // 3. Output the result
        cout << "You have: $" << countedAmount << endl << endl;
    }

    return 0;
}
