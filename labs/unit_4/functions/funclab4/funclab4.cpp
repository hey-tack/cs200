#include <iostream>
#include <string>
#include <cmath> // used for sqrt()
using namespace std;

// Function lab 4: Get distance
float GetDistance(
  float x1,
  float y1,
  float x2,
  float y2) {
    float dx = x2 - x1;
    float dy = y2 - y1;
    float dxSq = pow(dx, 2);
    float dySq = pow(dy, 2);
    float sum = dxSq + dySq;
    float distance = sqrt(sum);
    return distance;
}


int main() {
    cout << "DISTANCE" << endl << endl;

    while(true) {
        float x1, y1, x2, y2;

        cout << "1st coordinate pair, enter x and y: ";
        cin >> x1 >> y1;
        cout << endl;

        cout << "2nd coordinate pair, enter x and y: ";
        cin >> x2 >> y2;
        cout << endl;

        float distance = GetDistance (x1, y1, x2, y2);
        cout << "Distance: " << distance << endl;

        cout << endl << endl;
    }

    return 0;
}
