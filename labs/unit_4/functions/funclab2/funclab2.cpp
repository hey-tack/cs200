#include <iostream>
using namespace std;

// Function lab 2, Price and Tax
float AddTax(float dollar) {
    float taxAmount = dollar * 0.12;
    return dollar + taxAmount;
}

int main() {
    cout << "PRICE PLUS TAX" << endl << endl;

    cout << "Price: $" << 9.99
        << "\t with tax: $" << AddTax(9.99) << endl;

    cout << "Price: $" << 19.95
        << "\t with tax: $" << AddTax(19.95) << endl;

    cout << "Price: $" << 10.00
        << "\t with tax: $" << AddTax(10.00) << endl;

    return 0;
}


