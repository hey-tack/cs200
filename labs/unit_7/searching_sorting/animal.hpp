#ifndef _ANIMAL_HPP
#define _ANIMAL_HPP

#include <string>
using namespace std;

class Animal
{
    public:
    string id;
    string shelter;
    string type;
    string breed;
    string intakeDate;
    string intakeType;
    string intakeCondition;
    string group;

    void Display();

    Animal& operator=( const Animal& other );

    friend bool operator==( Animal& a1, Animal& a2 );
    friend bool operator!=( Animal& a1, Animal& a2 );
    friend bool operator<( Animal& a1, Animal& a2 );
    friend bool operator>( Animal& a1, Animal& a2 );
    friend bool operator<=( Animal& a1, Animal& a2 );
    friend bool operator>=( Animal& a1, Animal& a2 );
};

#endif
