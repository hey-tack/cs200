// Mad libs program, my stab at it.
// The basic idea is that you take raw input, usually asking for a
// specific type of work, grammatically speaking. sub it into a story.

#include <iostream>
#include <string>

using namespace std;

int main() {

    // Declare variables
    int number;
    float money;
    string noun, noun2;

    cout << "Enter a number: ";
    cin >> number;

    cout << "Enter a dollar amount: $";
    cin >> money;

    cout << "Enter a noun: ";
    cin >> noun;

    cout << "Enter another noun: ";
    cin >> noun2;

    cout << "Once there was a " << noun << " that had" << endl;
    cout << number << " " << noun2 << " and $" << money << endl;
    cout << endl;
    cout << "The End" << endl;

    return 0;
}
