// Average Grade

#include <iostream>
using namespace std;

int main() {
    float grade1, grade2, grade3, grade4;
    float gpa;

    char foo = 'b';
    char dee = "B";

    cout << "GPA CALCULATION PROGRAM" << endl;

    cout << "Please enter grade 1 (0 - 4): ";
    cin >> grade1;

    cout << "Please enter grade 2 (0 - 4): ";
    cin >> grade2;

    cout << "Please enter grade 3 (0 - 4): ";
    cin >> grade3;

    cout << "Please enter grade 4 (0 - 4): ";
    cin >> grade4;

    // Calculating the GPA
    gpa = (grade1 + grade2 + grade3 + grade4) / 4;

    cout << "Your GPA is: " << gpa << endl;
    cout << "Good work?" << endl;


    return 0;
}
