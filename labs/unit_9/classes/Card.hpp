#ifndef _CARD_HPP
#define _CARD_HPP

class Card {
    public:
    Card();

    void Setup(string newRank, char newSuit);
    void Display();
    void DisplayName();
    string GetRank();
    char GetSuit();

    private:
    string m_rank;
    char m_suit;
};

Card::Card() {
    // Do nothing I guess.
}

void Card::Setup(string newRank, char newSuit) {
    m_rank = newRank;
    m_suit = newSuit;
}

void Card::Display() {
    cout << m_rank << "-" << m_suit;
}

void Card::DisplayName() {
    // Display (with cout) the rank and suit together
    // written out to be readable. E.G, "Ace of Hearts".

    // Get the full version of the rank
    string fullRank;
    if (m_rank == "A") {
        fullRank = "Ace";
    } else if (m_rank == "1") {
        fullRank = "One";
    } else if (m_rank == "2") {
        fullRank = "Two";
    } else if (m_rank == "3") {
        fullRank = "Three";
    } else if (m_rank == "4") {
        fullRank = "Four";
    } else if (m_rank == "5") {
        fullRank = "Five";
    } else if (m_rank == "6") {
        fullRank = "Six";
    } else if (m_rank == "7") {
        fullRank = "Seven";
    } else if (m_rank == "8") {
        fullRank = "Eight";
    } else if (m_rank == "9") {
        fullRank = "Nine";
    } else if (m_rank == "10") {
        fullRank = "Ten";
    } else if (m_rank == "J") {
        fullRank = "Jack";
    } else if (m_rank == "Q") {
        fullRank = "Queen";
    } else if (m_rank == "K") {
        fullRank = "King";
    }

    // Get the full version of the suit
    string fullSuit;
    if (m_suit == 'H') {
        fullSuit = "Hearts";
    } else if (m_suit == 'D') {
        fullSuit = "Diamonds";
    } else if (m_suit == 'C') {
        fullSuit = "Clubs";
    } else if (m_suit == 'S') {
        fullSuit = "Spades";
    }

    cout << fullRank << " of " << fullSuit;

}

string Card::GetRank() {
    return m_rank;
}

char Card::GetSuit() {
    return m_suit;
}

#endif // _CARD_HPP
