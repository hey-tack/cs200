#include <iostream>
#include <cstdlib>
#include <time.h>
#include <array>

#include "Die.hpp"
#include "Student.h" // Accidentally inconsistent file extension naming. Oops.
#include "Deck.hpp"
#include "Recipe.hpp"

using namespace std;

int main() {
    int program;

    srand(time(NULL)); // Seeding RNG

    cout << "Choose a program to run" << endl;
    cout << "1. Dice Rolling Program" << endl;
    cout << "2. Students Program" << endl;
    cout << "3. Cards Program" << endl;
    cout << "4. Recipe Program" << endl;
    cout << "Run program #: ";
    cin >> program;
    cout << endl;

    if (program == 1) { // Dice rolling program
        bool isYahtzee = false;
        int rollAttempts = 0;
        Die die1 = Die();
        Die die2 = Die();
        Die die3 = Die();
        Die die4 = Die();
        Die die5 = Die();

        cout << "DICE ROLLING PROGRAM" << endl << endl;
        cout << "Let's see how many rolls it takes to score a Yahtzee! (5 of a kind)" << endl;
        while (!isYahtzee) {
            // roll our die
            int die1Result = die1.Roll();
            int die2Result = die2.Roll();
            int die3Result = die3.Roll();
            int die4Result = die4.Roll();
            int die5Result = die5.Roll();

            rollAttempts++;

            if (die1Result == die2Result &&
                die2Result == die3Result &&
                die3Result == die4Result &&
                die4Result == die5Result) {
                // All the die are equal. Yahtzee!!!
                cout << "After " << rollAttempts << " attempts, you finally rolled all " << die1Result << "'s. Yahtzee!!!" << endl;
                isYahtzee = true; // End the loop.
            }
        }
    } else if (program == 2) { // Students program
        bool addingStudents = true;
        int maxStudents = 10;
        int numberOfStudents = 0;
        Student students[10];

        cout << "STUDENTS PROGRAM" << endl << endl;

        do {
            bool addingGrades = true;
            string studentName;
            Student newStudent;
            char continueAddingStudentsAnswer;

            // Get a name for newStudent
            cout << "Please enter a name for student " << numberOfStudents + 1 << ": ";
            cin >> studentName;
            cout << endl;
            newStudent.Setup(studentName);

            int maxGrades = 10;
            int numberOfGrades = 0;
            do { // Add some grades for newStudent
                char continueGradingAnswer;
                float nextGrade;
                cout << "Please enter a grade in GPA format (a decimal between 1-4)" << endl;
                cout << "Grade: ";
                cin >> nextGrade;
                newStudent.AddGrade(nextGrade);

                numberOfGrades++;

                if (numberOfGrades == maxGrades) {
                    cout << "Maximum grades reached for student. Exiting Grading" << endl;
                    addingGrades = false;
                } else {
                    cout <<  "Do you want to add another grade? Y/N: ";
                    cin >> continueGradingAnswer;
                    cout << endl;

                    if (tolower(continueGradingAnswer) != 'y') {
                        addingGrades = false; // Done grading
                        cout << "Exiting grade entry." << endl;
                    }
                }
            } while (addingGrades);

            students[numberOfStudents] = newStudent;
            numberOfStudents++;

            if (numberOfStudents == maxStudents) {
                cout << "Max amount of students reached, Exiting student entry." << endl;
                addingStudents = false;
            } else {
                cout << "Do you want to add another student? Y/N: ";
                cin >> continueAddingStudentsAnswer;
                cout << endl;
                if (tolower(continueAddingStudentsAnswer) != 'y') {
                    addingStudents = false;
                    cout << "Exiting student entry" << endl;
                }
            }
        } while(addingStudents);

        // display student info
        cout << "NOW DISPLAYING STUDENT INFORMATION" << endl << endl;
        for (int i = 0; i < numberOfStudents; i++) {
            students[i].Display();
        }
    } else if (program == 3) { // Cards Program
        Deck myDeck;
        int suit, cardRank;

        cout << "CARD PROGRAM" << endl << endl;

        cout << "Enter a suit (0 - 3): ";
        cin >> suit;
        cout << endl;

        cout << "Enter a rank (0 - 12): ";
        cin >> cardRank;
        cout << endl;

        myDeck.DisplayCard(suit, cardRank);
        cout << endl;

        // Randomly chosen card
        suit = rand() % 4;
        cardRank = rand() % 12;

        myDeck.DisplayCard(suit, cardRank);
        cout << endl;
    } else if (program == 4) { // Recipe Program
        Recipe cookies;

        cookies.AddIngredient("Butter", 1, "cup");
        cookies.AddIngredient("White sugar", 1, "cup");
        cookies.AddIngredient("Eggs", 2, "items");
        cookies.AddIngredient("Vanilla", 2, "teaspoons");
        cookies.AddIngredient("BakingSoda", 1, "teaspoon");

        cookies.SetInstructions("Preheat oven to 350 F, combine ingredients in bowl, put on cookie sheet, bake for 10 minutes.");

        cookies.Display();
    }
    //etc

    return 0;
}
