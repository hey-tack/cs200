#ifndef _STUDENT_HPP
#define _STUDENT_HPP

#include <string>
using namespace std;

class Student {
    public:
    Student();

    void Setup(string newName);
    void AddGrade(float score);
    float GetGpa();
    string GetName();
    void Display();

    private:
    string fullName;
    float grades[10];
    int totalGrades;
};

Student::Student() {
    totalGrades = 0;
}

void Student::Setup(string newName) {
    fullName = newName;
}

void Student::AddGrade(float score) {
    if (score < 0 || score > 4) {
        cout << "Score is out of range!" << endl;
        return; // Leave the function
    }

    if (totalGrades == 10) {
        cout << "Grades array is full!" << endl;
        return; // Leave the function
    }

    grades[totalGrades] = score;
    totalGrades++;
}

float Student::GetGpa() {
    float total = 0;
    for (int i = 0; i < totalGrades; i++) {
        total += grades[i];
    }
    return total / totalGrades;
}

string Student::GetName() {
    return fullName;
}

void Student::Display() {
    cout << GetName() << ": " << GetGpa() << endl;
}

#endif // _STUDENT_HPP
