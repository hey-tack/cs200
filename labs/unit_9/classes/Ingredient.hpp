#ifndef _INGREDIENT_HPP
#define _INGREDIENT_HPP

#include <string>
using namespace std;

class Ingredient {
    public:
    void Setup(string name, float amount, string unit);
    void Display();

    private:
    string m_name;
    string m_unit;
    float m_amount;
};

void Ingredient::Setup(string name, float amount, string unit) {
    m_name = name;
    m_unit = unit;
    m_amount = amount;
}

void Ingredient::Display() {
    cout << m_name << ", " << m_amount << " " << m_unit;
}

#endif // _INGREDIENT_HPP
