#ifndef _DIE_HPP
#define _DIE_HPP

#include <cstdlib> // for rand()

struct Die
{
    // Constructors
    Die();
    Die(int sideCount);

    // Method
    int Roll();

    // Variable
    int sides;
};

// I guess the :: is for defining functions outside
// of the scope of the class / struct. Interesting.
Die::Die() {
    sides = 6;
}

Die::Die(int sideCount) {
    sides = sideCount;
}

int Die::Roll() {
    if (sides == 10) {
        return rand() % sides;
    } else {
        return rand() % sides + 1;
    }
}

#endif // _DIE_HPP
