#ifndef _DECK_HPP
#define _DECK_HPP

#include <string>
#include "Card.hpp"

using namespace std;

class Deck {
    public:
    Deck();

    void CreateDeck();
    void DisplayCard(int s, int r);

    private:
    Card m_cards[4][13];
};

Deck::Deck() {
    CreateDeck();
}

void Deck::CreateDeck() {
    const int SUITS = 4;
    const int RANKS = 13;

    const char suits[] = { 'C', 'D', 'H', 'S'};
    const string ranks[] = {"A", "1", "2", "3", "4",
                            "5", "6", "7", "8", "9",
                            "10", "J", "Q", "K"};

    for (int s = 0; s < SUITS; s++) {
        for (int r = 0; r < RANKS; r++) {
            // Getting C, D, H, or S, from the array
            char suit = suits[s];

            // Getting rank name from the array
            string cardRank = ranks[r];

            // Setting up the card
            m_cards[s][r].Setup(cardRank, suit);
        }
    }
}

void Deck::DisplayCard(int s, int r) {
    cout << "Card [" << s << "][" << r << "] is : ";
    m_cards[s][r].DisplayName();
    cout << endl;
}

#endif // _DECK_HPP
