
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

/** dDisplay two options: Add new to-do item, or quit */
void DisplayMainMenu() {
    cout << "VOTING" << endl << endl;
    cout << "-----------------" << endl;
    cout << "Vote for your favorite food item:" << endl;
    cout << "1. Frosted Flakes" << endl;
    cout << "2. Mom's Spaghetti" << endl;
    cout << "3. M&M's Chocolate Candies" << endl;
    cout << "4. Slim Jims" << endl << endl;
    cout << "Or quit the program with: " << endl;
    cout << "5. Quit" << endl << endl;
}

/**
Get the user's input, validate whether it is between min
and max, if the value is invalid, ask the user to input
it again (using a while loop). Once the user's input is
valid, return that value.
**/
int GetChoice(int min, int max) {
    int input = 0; // Initialize to zero for safety.

    cout << "Choice: ";
    cin >> input;
    cout << endl;

    // I noticed that this breaks horribly
    // when non-int values are used. Not
    // sure how to fix.
    while (input < min || input > max) {
        cout << "Invalid value, try again." << endl << endl;

        cout << "Choice: ";
        cin >> input;
        cout << endl;
    }
}

/**
Get a line of text from the user, and return this info
*/
string GetToDoItem() {

    cout << "Please enter a to-do item." << endl << endl;
    cout << ">> ";
    cin.ignore();
    string todoItemInput;
    getline(cin, todoItemInput);
    return todoItemInput;
}

int main() {
    ofstream output("votes.txt");
    bool done = false;
    int menuChoice;

    // Variables for totals
    int frostedFlakesVotes = 0;
    int momsSpaghettiVotes = 0;
    int mnmsChocolateCandiesVotes = 0;
    int slimJimsVotes = 0;

    while (!done) {
        DisplayMainMenu();
        menuChoice = GetChoice(1,5);
        if (menuChoice == 1) { // Frosted Flakes
            frostedFlakesVotes++;
        } else if (menuChoice == 2) { // Mom's Spaghetti
            momsSpaghettiVotes++;
        } else if (menuChoice == 3) { // M&M's Chocolate Candies
            mnmsChocolateCandiesVotes++;
        } else if (menuChoice == 4) { // Slim Jims
            slimJimsVotes++;
        } else if (menuChoice == 5) { // Quit
            done = true;
            cout << "Bye! Results will now be output to votes.txt" << endl << endl;
        }
    }

    // Output final results
    output << "POLL RESULTS" << endl << endl;
    output << "Frosted Flakes: \t\t" << frostedFlakesVotes << " votes" << endl;
    output << "Mom's Spaghetti: \t\t" << momsSpaghettiVotes << " votes" << endl;
    output << "M&M's Chocolate Candies: \t" << mnmsChocolateCandiesVotes << " votes" << endl;
    output << "Slim Jims: \t\t\t" << slimJimsVotes << " votes" << endl;

    output.close();

    return 0;
}
