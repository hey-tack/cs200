#include <iostream>
#include <string>
using namespace std;

// ----------------------------------------------------------------
// Function declarations
int GetPerimeter( int width, int length );
char GetFirstLetter( string text );
float Withdraw( float balance, float amount );

// ----------------------------------------------------------------
// Function declarations - tests
void Test_GetPerimeter();
void Test_GetFirstLetter();
void Test_Withdraw();

// ----------------------------------------------------------------
// Main
int main()
{
    // Run tests
    Test_GetPerimeter();
    Test_GetFirstLetter();
    Test_Withdraw();

    return 0;
}

// ----------------------------------------------------------------
// Function definitions

/**
    Calculates and returns the perimeter of a rectangle.
    (2 x width + 2 x length)
*/
int GetPerimeter( int width, int length )
{
    return (2 * width) + (2 * length);
}

/**
    Gets the first letter of a string.
    Use the subscript operator with the string:
    text[2] is the item at index 2.
*/
char GetFirstLetter( string text )
{
    return text[0];
}

/**
    Adjusts the balance based on the amount withdrawn
    and returns the new balance.
    If the amount to withdraw is more than the balance,
    no change is made.
*/
float Withdraw( float balance, float amount )
{
    if (amount > balance) {
        return balance;
    } else {
        return balance - amount;
    }
}

// ----------------------------------------------------------------
// Function definitions - tests
void Test_GetPerimeter()
{
    cout << endl << "------------------------------------" << endl;
    cout << "Test_GetPerimeter:" << endl;

    int width, length;
    int expectedOutput;
    int actualOutput;

    // Test 1
    width = 7;
    length = 3;
    expectedOutput = 20;
    actualOutput = GetPerimeter( width, length );

    cout << endl << "Test 1... ";
    if ( actualOutput == expectedOutput )
    {
        cout << "PASS" << endl;
    }
    else
    {
        cout << "FAIL" << endl
            << " width: " << width << ", length: " << length << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
    }

    // Test 2
    width = 8;
    length = 5;
    expectedOutput = 26;
    actualOutput = GetPerimeter( width, length );

    cout << endl << "Test 2... ";
    if ( actualOutput == expectedOutput )
    {
        cout << "PASS" << endl;
    }
    else
    {
        cout << "FAIL" << endl
            << " width: " << width << ", length: " << length << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
    }
}

void Test_GetFirstLetter()
{
    cout << endl << "------------------------------------" << endl;
    cout << "Test_GetFirstLetter:" << endl;

    string text;
    char expectedOutput;
    char actualOutput;

    // Test 1
    text = "ABC";
    expectedOutput = 'A';
    actualOutput = GetFirstLetter( text );

    cout << endl << "Test 1... ";
    if ( actualOutput == expectedOutput )
    {
        cout << "PASS" << endl;
    }
    else
    {
        cout << "FAIL" << endl
            << " text: " << text << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
    }

    // Test 2
    text = "xyz";
    expectedOutput = 'x';
    actualOutput = GetFirstLetter( text );

    cout << endl << "Test 2... ";
    if ( actualOutput == expectedOutput )
    {
        cout << "PASS" << endl;
    }
    else
    {
        cout << "FAIL" << endl
            << " text: " << text << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
    }
}

void Test_Withdraw()
{
    cout << endl << "------------------------------------" << endl;
    cout << "Test_Withdraw:" << endl;

    float balance, amount;
    float expectedOutput;
    float actualOutput;

    // Test 1
    balance = 100;
    amount = 100;
    expectedOutput = 0;
    actualOutput = Withdraw( balance, amount );

    cout << endl << "Test 1... ";
    if ( actualOutput == expectedOutput )
    {
        cout << "PASS" << endl;
    }
    else
    {
        cout << "FAIL" << endl
            << " balance: " << balance << ", amount: " << amount << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
    }

    // Test 2
    balance = 100;
    amount = 500;
    expectedOutput = 100;   // expect no change
    actualOutput = Withdraw( balance, amount );

    cout << endl << "Test 2... ";
    if ( actualOutput == expectedOutput )
    {
        cout << "PASS" << endl;
    }
    else
    {
        cout << "FAIL" << endl
            << " balance: " << balance << ", amount: " << amount << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
    }

    // Test 3
    balance = 0;
    amount = 10;
    expectedOutput = 0;   // expect no change
    actualOutput = Withdraw( balance, amount );

    cout << endl << "Test 3... ";
    if ( actualOutput == expectedOutput )
    {
        cout << "PASS" << endl;
    }
    else
    {
        cout << "FAIL" << endl
            << " balance: " << balance << ", amount: " << amount << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
    }

    // Test 4
    balance = 20;
    amount = 10;
    expectedOutput = 10;
    actualOutput = Withdraw( balance, amount );

    cout << endl << "Test 4... ";
    if ( actualOutput == expectedOutput )
    {
        cout << "PASS" << endl;
    }
    else
    {
        cout << "FAIL" << endl
            << " balance: " << balance << ", amount: " << amount << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
    }
}

