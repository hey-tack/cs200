#include <iostream>
#include <string>
#include <fstream>
using namespace std;

/** dDisplay two options: Add new to-do item, or quit */
void DisplayMainMenu() {
    cout << "-----------------" << endl;
    cout << "1. New to-do item" << endl;
    cout << "2. Save and quit" << endl << endl;
}

/**
Get the user's input, validate whether it is between min
and max, if the value is invalid, ask the user to input
it again (using a while loop). Once the user's input is
valid, return that value.
**/
int GetChoice(int min, int max) {
    int input = 0; // Initialize to zero for safety.

    cout << ">> ";
    cin >> input;
    cout << endl;

    // I noticed that this breaks horribly
    // when non-int values are used. Not
    // sure how to fix.
    while (input < min || input > max) {
        cout << "Invalid value, try again." << endl << endl;

        cout << ">> ";
        cin >> input;
        cout << endl;
    }
}

/**
Get a line of text from the user, and return this info
*/
string GetToDoItem() {

    cout << "Please enter a to-do item." << endl << endl;
    cout << ">> ";
    cin.ignore();
    string todoItemInput;
    getline(cin, todoItemInput);
    return todoItemInput;
}

int main() {
    ofstream output("todo.txt");
    int counter = 1;
    bool done = false;
    int menuChoice;
    string todoItem;

    output << "TO DO" << endl << endl;

    while (!done) {
        DisplayMainMenu();
        menuChoice = GetChoice(1,2);
        if (menuChoice == 1) {
            todoItem = GetToDoItem();
            output << counter << ". " << todoItem << endl;
            counter++;
        } else if (menuChoice == 2) {
            done = true;
        }
    }

    output.close();

    return 0;
}
